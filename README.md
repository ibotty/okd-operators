# OKD operators not distributed without license by RedHat

For now only the local storage operator


## Hacking

### Veneer Config
```
opm alpha render-veneer semver -o yaml local-storage-operator.veneer.yaml  > okd-catalog/catalog.yaml
podman build -f okd-catalog.Dockerfile -t quay.io/ibotty/okd-operators:latest
podman push quay.io/ibotty/okd-operators:latest
```

### Generate Bundle Image for local storage operator
```
cd openshift-local-storage-operator/config; git checkout release-4.11;
fastmod '(quay.io/openshift/origin-local-storage-[a-z]+)($|:latest)' '${1}:4.11' manifests/
podman build -f bundle.Dockerfile -t quay.io/ibotty/openshift-local-storage-bundle:4.11 
podman push quay.io/ibotty/openshift-local-storage-bundle:4.11
```
