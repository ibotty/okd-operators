# The base image is expected to contain
# /bin/opm (with a serve subcommand) and /bin/grpc_health_probe
FROM quay.io/operator-framework/opm:latest

# Configure the entrypoint and command
ENTRYPOINT ["/bin/opm"]
CMD ["serve", "/configs"]

# Generate dependency graph
RUN opm alpha render-veneer semver local-storage-operator.veneer.yaml -o yaml > okd-catalog/catalog.yaml

# Copy declarative config root into image at /configs
ADD okd-catalog /configs

# Set DC-specific label for the location of the DC root directory
# in the image
LABEL operators.operatorframework.io.index.configs.v1=/configs
